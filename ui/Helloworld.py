#!/usr/bin/env python
# encoding: utf-8


"""
@author:    0604hx
@license:   MIT 
@contact:   zxingming@foxmail.com
@site:      https://github.com/0604hx
@software:  PyCharm Community Edition
@project:   qt5-study
@file:      Helloworld.py
@time:      18/2/20 上午12:02
"""
import sys

from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton


class ExampleUI(QWidget):
    def __init__(self):
        super().__init__()

        self.ICON_HSQ = QIcon("../static/hashiqi.png")

        self.init()

    def init(self):
        self.resize(800, 480)
        self.setWindowTitle("第一个 PyQt5 程序")
        self.setWindowIcon(self.ICON_HSQ)
        self.setToolTip("这是 ExampleUI(QWidget) 的提示信息")

        self.__setup__()

        self.show()

    def __setup__(self):
        btn = QPushButton("Button To Exit", self)
        btn.move(20, 20)
        btn.setToolTip("点击此按钮以退出")
        btn.setIcon(self.ICON_HSQ)
        btn.clicked.connect(QApplication.instance().quit)
        btn.resize(btn.sizeHint())

        self.btn = btn


if __name__ == '__main__':
    app = QApplication(sys.argv)

    ex = ExampleUI()

    sys.exit(app.exec_())
